PROJECT_NAME = nanvel
APPS = core weblog uploader
TEST_APPS = core weblog uploader

test:
	python manage.py test $(TEST_APPS)

run:
	python manage.py runserver

shell:
	python manage.py shell

syncdb:
	python manage.py syncdb

migrate:
	python manage.py migrate

mailserver:
	python -m smtpd -n -c DebuggingServer 0.0.0.0:1025

collectstatic:
	python manage.py collectstatic

manage:
	python manage.py $(CMD)

elasticsearch:
	elasticsearch -f -D es.config=/usr/local/Cellar/elasticsearch/1.1.0/config/elasticsearch.yml
