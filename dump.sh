#!/bin/sh

DESC="Create database dump"

cd $(dirname $0)
if [ -f dump.def ] ; then
    . ./dump.def ;
else
    echo "error=no $(pwd)/dump.def !"
    exit 1
fi

filename="$BACKUPS_DIR/$DB_NAME_"`eval date +%Y_%m_%d`".dump.gz"
pg_dump $DB_NAME --username $DB_USER | gzip -c > $filename
cp $filename "dump.gz"
