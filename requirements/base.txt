django>=1.5,< 1.6
PIL
pep8
django-pipeline
south
django-any
gdata==2.0.17
mock==1.0.1
django-haystack==2.1.0
pyelasticsearch==0.6.1
