from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.sitemaps import FlatPageSitemap
from django.conf import settings

from nanvel.settings.utils import absolute_path
from nanvel.sitemaps import WeblogSitemap


admin.autodiscover()

sitemaps = {
    'weblog': WeblogSitemap,
    'flatpages': FlatPageSitemap,
}

urlpatterns = patterns('',
    url(r'^$', 'nanvel.apps.weblog.views.list', name='weblog_list'),
    url(r'^uploader/', include('nanvel.apps.uploader.urls')),
    url(r'^weblog/', include('nanvel.apps.weblog.urls')),
    url(r'^%s' % (settings.ADMIN_URL), include(admin.site.urls)),
    url(r'^robots\.txt$', 'nanvel.apps.core.views.robots', name='robots'),
)

urlpatterns += patterns('django.contrib.sitemaps.views',
    (r'^sitemap\.xml$', 'index', {'sitemaps': sitemaps}),
    (r'^sitemap-(?P<section>.+)\.xml$', 'sitemap', {'sitemaps': sitemaps}),
)

urlpatterns += patterns('django.contrib.flatpages.views',
    url(r'^nanvel/$', 'flatpage', {'url': '/nanvel/'}, name='nanvel'),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$',
                'django.views.static.serve',
                {'document_root': absolute_path('media')}
        ),
    )

urlpatterns += patterns('django.contrib.flatpages.views',
    (r'^(?P<url>.*)$', 'flatpage'),
)

handler500 = 'nanvel.apps.core.views.handler500'
handler404 = 'nanvel.apps.core.views.handler404'
