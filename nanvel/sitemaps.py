from django.contrib.sitemaps import Sitemap

from nanvel.apps.weblog.models import WeblogItem


class WeblogSitemap(Sitemap):
    changefreq = 'monthly'
    priority = 0.5

    def items(self):
        return WeblogItem.objects.active()

    def lastmod(self, obj):
        return obj.last_modify
