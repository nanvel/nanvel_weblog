from django.conf import settings
from django.db import models
from django.dispatch import receiver
from django.template.defaultfilters import truncatewords, truncatechars

from nanvel.apps.core.utils import send_mail


class Tag(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=50)

    def __unicode__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return ('nanvel.apps.weblog.views.list', (), {'tag': self.slug})

    class Meta:
        ordering = ('name',)


class WeblogItemManager(models.Manager):

    def active(self):
        """
        Return weblog items with status == ACTIVE
        """
        return super(WeblogItemManager, self).get_query_set(
            ).filter(status=WeblogItem.ACTIVE)


class WeblogItem(models.Model):

    ACTIVE = u'A'
    FREEZE = u'F'
    DIVIDER = settings.POST_CUT_DIVIDER

    STATUS_CHOICES = (
        (ACTIVE, 'Active'),
        (FREEZE, 'Freeze'), 
    )

    content = models.TextField()
    title = models.CharField(max_length=255)
    slug = models.SlugField(max_length=50, unique=True, null=False)
    description = models.CharField(max_length=1000, default=u'')
    keywords = models.CharField(max_length=255, default=u'')
    tags = models.ManyToManyField(Tag, related_name='posts')
    timestamp = models.DateTimeField(auto_now_add=True)
    last_modify = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=1, choices=STATUS_CHOICES)
    show_ad = models.BooleanField(default=False)

    objects = WeblogItemManager()

    class Meta:
        ordering = ('-timestamp',)

    def __unicode__(self):
        return self.title

    def has_full(self):
        return self.content.find(self.DIVIDER) != -1

    @property
    def comments_count(self):
        count = self.comments.filter(status=Comment.GOOD).count()
        return ' (%d)' % count if count else ''

    @property
    def cut(self):
        if self.content.find(self.DIVIDER) == -1:
            return self.content
        return self.content[:self.content.find(self.DIVIDER)]

    @property
    def part2(self):
        if self.content.find(self.DIVIDER) == -1:
            return ''
        return self.content[self.content.find(self.DIVIDER):]

    @models.permalink
    def get_absolute_url(self):
        return ('nanvel.apps.weblog.views.full', (), {'slug': self.slug})


class CommentManager(models.Manager):

    def active(self):
        """
        Return comments with status == GOOD
        """
        return super(CommentManager, self).get_query_set(
            ).filter(status=Comment.GOOD)


class Comment(models.Model):

    UNMODERATED = u'U'
    GOOD = u'G'
    TRASH = u'T'

    STATUS_CHOICES = (
        (UNMODERATED, 'Unmoderated'),
        (GOOD, 'Good'),
        (TRASH, 'Trash'),
    )

    weblog_item = models.ForeignKey(WeblogItem, related_name=u'comments')
    content = models.CharField(verbose_name=u'Comment', max_length=1000)
    timestamp = models.DateTimeField(auto_now_add=True)
    email = models.EmailField(
        blank=True, null=True, verbose_name=u'Email',
        help_text=u'Not required.')
    name = models.CharField(max_length=50, verbose_name=u'Name')
    site = models.URLField(
        blank=True, null=True, verbose_name=u'Site',
        help_text=u'Not required.')
    status = models.CharField(
        max_length=1, choices=STATUS_CHOICES,
        default=UNMODERATED)

    objects = CommentManager()

    def __unicode__(self):
        return truncatechars(truncatewords(self.content, 5), 50)

    class Meta:
        ordering = ('timestamp',)


@receiver(models.signals.post_save, sender=Comment)
def comment_handler(sender, **kwargs):
    if kwargs['created']:
        emails = [email[1] for email in settings.MANAGERS]
        ctx = {'comment': kwargs['instance']}
        send_mail('emails/new_comment', ctx, emails)
