from haystack.forms import SearchForm

from django import forms

from nanvel.apps.core.widgets import NicEditAdminWidget

from .models import Comment, WeblogItem


class WeblogSearchForm(SearchForm):

    def no_query_found(self):
        return self.searchqueryset.all()


class CommentAdminForm(forms.ModelForm):

    class Meta:
        model = Comment
        widgets = {
            'content': forms.Textarea,
        }


class WeblogItemAdminForm(forms.ModelForm):

    class Meta:
        model = WeblogItem
        widgets = {
            'description': forms.Textarea,
            'keywords': forms.Textarea,
            'content': NicEditAdminWidget(attrs={'style': 'width: 800px;'}),
        }


class CommentForm(forms.ModelForm):

    weblog_item = forms.CharField(widget=forms.HiddenInput)

    class Meta:
        model = Comment
        fields = ['weblog_item', 'content', 'email', 'name', 'site']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'name'}),
            'email': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'email (not required)'}),
            'site': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'site (not required)'}),
            'content': forms.Textarea(attrs={'class': 'form-control', 'rows': 3}),
        }

    def clean_weblog_item(self):
        weblog_item = self.cleaned_data['weblog_item']
        try:
            return WeblogItem.objects.active().get(slug=weblog_item)
        except WeblogItem.DoesNotExist:
            raise forms.ValidationError('Can\'t find specified weblog item.')
