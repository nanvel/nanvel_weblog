def get_page_number(data):
    n = data.get('page', 1)
    try:
        n = int(n)
    except ValueError:
        n = 1
    return n
