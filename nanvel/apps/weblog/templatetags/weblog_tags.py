from django import template
from django.conf import settings
from django.contrib.flatpages.models import FlatPage
from django.contrib.sites.models import Site
from django.utils.html import escape

from nanvel.apps.weblog.models import Tag, WeblogItem


register = template.Library()


@register.simple_tag
def tags():
    MIN_SIZE = 12.
    DELTA_SIZE = 10.
    html = u''
    max_count = 0
    for t in Tag.objects.all():
        c = t.posts.count()
        if c > max_count:
            max_count = c
    for t in Tag.objects.all():
        size = round(MIN_SIZE + t.posts.count() * DELTA_SIZE / max_count
                                            ) if max_count else MIN_SIZE
        html += '<a href="%s" style="font-size:%dpx;">%s</a> ' % (
                                            t.get_absolute_url(),
                                            size,
                                            t.name)
    return html


@register.simple_tag
def projects():
    site = Site.objects.get_current()
    flatpages = FlatPage.objects.filter(
                    sites=site,
                    url__startswith=settings.PROJECTS_URL)
    if not flatpages.exists():
        return u''
    html = u'<ul>'
    for fp in flatpages:
        html += '<li><a href="%s">%s</a>' % (fp.url, escape(fp.title))
    html += '</ul>'
    return html


@register.simple_tag
def latest_updates():
    latest = WeblogItem.objects.active().order_by('-last_modify')[:10]
    html = u'<ul>'
    for l in latest:
        html += '<li><a href="%s">%s</a>' % (l.get_absolute_url(), escape(l.title))
    html += '</ul>'
    return html


@register.simple_tag
def post_tags(post):
    html = u''
    for t in post.tags.all():
        html += '<i class="icon-tag"></i> <a href="%s">%s</a> ' % (
            t.get_absolute_url(), escape(t.name))
    return html


# template filters


@register.filter
def urlget(value, var_name):
    if not value:
        return u''
    return u'&%s=%s' % (var_name, value)
