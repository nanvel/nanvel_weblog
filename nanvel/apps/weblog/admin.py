from django.conf import settings
from django.contrib import admin, messages
from django.contrib.sitemaps import ping_google
from django.http import HttpResponseRedirect

from .models import WeblogItem, Comment, Tag
from .forms import CommentAdminForm, WeblogItemAdminForm


class ButtonableModelAdmin(admin.ModelAdmin):
    """
    A subclass of this admin will let you add buttons (like history) in the
    change view of an entry.
    More: https://djangosnippets.org/snippets/1016/
    admin/change_form.html template is modified.
    """
    buttons = []

    def change_view(self, request, object_id, extra_context={}):
        extra_context['buttons'] = self.buttons
        return super(ButtonableModelAdmin, self).change_view(
            request=request, object_id=object_id,
            extra_context=extra_context)

    def button_view_dispatcher(self, request, object_id, command):
        obj = self.model._default_manager.get(pk=object_id)
        return getattr(self, command)(request, [obj]) or HttpResponseRedirect(request.META['HTTP_REFERER'])

    def get_urls(self):

        from django.conf.urls import patterns, url
        from django.utils.functional import update_wrapper

        def wrap(view):
            def wrapper(*args, **kwargs):
                return self.admin_site.admin_view(view)(*args, **kwargs)
            return update_wrapper(wrapper, view)

        info = self.model._meta.app_label, self.model._meta.module_name

        return patterns('',
            *(url(r'^(\d+)/(%s)/$' % but[0], wrap(self.button_view_dispatcher)) for but in self.buttons)
        ) + super(ButtonableModelAdmin, self).get_urls()


class WeblogItemAdmin(ButtonableModelAdmin):
    list_display = ('title', 'timestamp', 'status', 'show_ad',)
    list_filter = ('status', 'show_ad',)
    fields = ('title', 'slug', 'description', 'keywords', 'content',
                    'tags', 'status', 'show_ad', 'timestamp', 'last_modify',)
    readonly_fields = ('timestamp', 'last_modify',)
    search_fields = ('title',)
    filter_horizontal = ('tags',)
    prepopulated_fields = {'slug': ('title',)}
    list_per_page = 20
    form = WeblogItemAdminForm

    def _ping_google(self, request, queryset):
        try:
            ping_google()
            messages.success(
                request,
                'Sitemaps were submitted.')
        except Exception:
            messages.warning(
                request,
                'Sitemaps were not submitted.')

    _ping_google.short_description = 'Ping google'

    buttons = [(_ping_google.func_name, _ping_google.short_description)]

    class Media:
        js = (
            '%sjs/nicedit.js' % settings.STATIC_URL,
            '%sjs/admin_rte.js' % settings.STATIC_URL,)
        css = {'all': ('%scss/raw/admin_rte.css' % settings.STATIC_URL,)}


class CommentAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'name', 'timestamp', 'status',)
    list_filter = ('status', 'timestamp',)
    fields = ('weblog_item', 'content', 'name', 'email', 'site',
                                                'status', 'timestamp',)
    readonly_fields = ('timestamp', 'weblog_item',)
    search_fields = ('name', 'email', 'site',)
    form = CommentAdminForm


class TagAdmin(admin.ModelAdmin):
    list_display = ('name',)
    prepopulated_fields = {'slug': ('name',)}


admin.site.register(WeblogItem, WeblogItemAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(Tag, TagAdmin)
