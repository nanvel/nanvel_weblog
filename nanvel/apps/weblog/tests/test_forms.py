from django.test import TestCase

from django_any import any_model

from ..models import Comment, WeblogItem
from ..forms import CommentForm


class WeblogFormsTestCase(TestCase):

    def test_comment_form(self):

        wia = any_model(WeblogItem, status=WeblogItem.ACTIVE)
        wif = any_model(WeblogItem, status=WeblogItem.FREEZE)

        test_data_set = [
            { # all fields filled
                'weblog_item': wia.slug,
                'content': 'hello!',
                'name': 'Yui',
                'email': 'yui@mail.com',
                'site': 'http://somesite.com',
                'is_valid': True},
            { # all required fields filled
                'weblog_item': wia.slug,
                'content': 'hello!',
                'name': 'Yui',
                'is_valid': True},
            { # empty request
                'is_valid': False},
            { # not active weblog item
                'weblog_item': wif.slug,
                'content': 'hello!',
                'name': 'Yui',
                'is_valid': False},
            { # bad email
                'weblog_item': wia.slug,
                'content': 'hello!',
                'name': 'Yui',
                'email': 'badenmail.com',
                'is_valid': False},
            { # bad site name
                'weblog_item': wia.slug,
                'content': 'hello!',
                'name': 'Yui',
                'site': 'some bad site name',
                'is_valid': False},
        ]

        for data in test_data_set:
            form = CommentForm(data)
            self.assertEqual(form.is_valid(), data['is_valid'])

        # test save method
        data = test_data_set[0]
        form = CommentForm(data)
        self.assertTrue(form.is_valid())
        form.save()
        comment = Comment.objects.order_by('-id')[0]
        self.assertEqual(comment.content, data['content'])
        self.assertEqual(comment.status, Comment.UNMODERATED)
        
