from django_any import any_model

from django.conf import settings
from django.core.urlresolvers import reverse
from django.template.defaultfilters import slugify
from django.test import TestCase
from django.utils import simplejson

from ..models import WeblogItem, Comment


class WeblogViewsTestCase(TestCase):

    divider = settings.POST_CUT_DIVIDER
    part1 = 'Some content %d'
    part2 = ' Part 2'
    content = '%s%s%s' % (part1, divider, part2)
    title = 'some title %d'

    def create_weblog_items(self, n=1):
        """
        Helper for weblog items creation
        """
        posts = []
        for i in xrange(n):
            wi = WeblogItem(
                content=self.content % i, title=self.title % i,
                slug=slugify(self.title % i), status=WeblogItem.ACTIVE)
            wi.save()
            posts.append(wi)
        return posts

    def get_first_post(self):
        """
        Helper for obtaining first weblog post item
        """
        return WeblogItem.objects.order_by('-id')[0]

    def test_weblog_list(self):
        self.create_weblog_items(n=1)
        self.assertEqual(WeblogItem.objects.count(), 1)
        post = self.get_first_post()
        r = self.client.get(reverse('weblog_list'))
        self.assertEqual(r.status_code, 200)
        self.assertContains(r, post.title)
        self.assertContains(r, post.cut)
        # test not active post
        post.status = WeblogItem.FREEZE
        post.save()
        r = self.client.get(reverse('weblog_list'))
        self.assertEqual(r.status_code, 200)
        self.assertNotContains(r, post.title)
        self.assertNotContains(r, post.cut)

    def test_weblog_full(self):
        self.create_weblog_items(n=1)
        post = self.get_first_post()
        r = self.client.get(
            reverse('weblog_post_full', kwargs={'slug': post.slug}))
        self.assertEqual(r.status_code, 200)
        self.assertContains(r, post.title)

    def test_comments_list(self):
        self.create_weblog_items(n=1)
        post = self.get_first_post()
        comment = any_model(
            Comment, status=Comment.UNMODERATED,
            weblog_item=post, name=u'adzusa',
            email=u'adzusa@mail.com', site=u'http://adzusasite.com',
            content=u'Some adzusa comment.')
        r = self.client.get(
            reverse('weblog_post_full', kwargs={'slug': post.slug}))
        # Don't moderated comments are invisible
        self.assertNotContains(r, comment.content)
        comment.status = Comment.GOOD
        comment.save()
        r = self.client.get(
            reverse('weblog_post_full', kwargs={'slug': post.slug}))
        self.assertContains(r, comment.content)
        self.assertContains(r, comment.name)
        self.assertNotContains(r, comment.email)
        self.assertContains(r, comment.site)

    def test_weblog_ajax_full(self):
        self.create_weblog_items(n=1)
        post = self.get_first_post()
        r = self.client.get(
            reverse('weblog_post_full', kwargs={'slug': post.slug}),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.content, post.part2)

    def test_pagination(self):
        self.create_weblog_items(n=7)
        with self.settings(POSTS_PER_PAGE=5):
            r = self.client.get(reverse('weblog_list'), {'page': 1})
            self.assertEqual(r.status_code, 200)
            r = self.client.get(reverse('weblog_list'), {'page': 3})
            self.assertEqual(r.status_code, 404)

    def test_search(self):
        post1, post2 = self.create_weblog_items(2)
        post1.title = 'abcxx 123'
        post1.save()
        post2.title = 'defxx 123'
        post2.save()
        r = self.client.get(reverse('weblog_list'), {'q': 'abcxx'})
        self.assertEqual(r.context['q'], 'abcxx')
        self.assertEqual(r.status_code, 200)
        # also displayed in latest posts
        self.assertContains(r, 'abcxx', 5)
        self.assertContains(r, 'defxx', 2)
        r = self.client.get(reverse('weblog_list'), {'q': '123'})
        self.assertEqual(r.status_code, 200)
        self.assertContains(r, 'abcxx', 2)
        self.assertContains(r, 'defxx', 2)

    def test_description_and_keywords(self):
        """
        description and keywords for post pages saves in Weblog/item model.
        description and keywords for other pages is same and hardcoded in base.html.
        """
        wi = self.create_weblog_items()[0]
        wi.description = 'Some doscription'
        wi.keywords = 'keyword1, keyword2'
        wi.save()

        # index page - standard description and keywords
        r = self.client.get(reverse('weblog_list'))
        self.assertEqual(r.status_code, 200)
        self.assertNotContains(r, wi.description)
        self.assertNotContains(r, wi.keywords)
        self.assertContains(r, 'posts list')

        # post page
        r = self.client.get(reverse(
            'weblog_post_full', kwargs={'slug': wi.slug}))
        self.assertEqual(r.status_code, 200)
        self.assertContains(r, wi.description)
        self.assertContains(r, wi.keywords)
        self.assertNotContains(r, 'Bookmarks about Python, Django')


class CommentViewsTestCase(TestCase):

    comment_data = {
        'name': u'mio',
        'email': u'mio@mail.com',
        'site': u'miosite.com',
        'content': u'Some comment'
    }

    def create_post(self):
        return any_model(WeblogItem, status=WeblogItem.ACTIVE)

    def create_post_and_comment(self):
        wi = self.create_post()
        comment_data = self.comment_data
        comment_data.update({'weblog_item': wi})
        comment = any_model(Comment, **comment_data)
        return comment

    def test_add_comment(self):

        wi = self.create_post()

        comment_data = {
            'weblog_item': wi.slug, 'name': u'erio',
            'email': u'erio@mail.com', 'site': u'eiosite.com',
            'content': u'Some comment.'
        }

        # try get request
        r = self.client.get(reverse('weblog_comment_add'), comment_data)
        self.assertEqual(r.status_code, 404)
        # try not ajax request
        r = self.client.post(reverse('weblog_comment_add'), comment_data)
        self.assertEqual(r.status_code, 404)

        self.assertFalse(
            Comment.objects.filter(name=self.comment_data['name']).exists())
        r = self.client.post(
            reverse('weblog_comment_add'),
            comment_data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(r.status_code, 200)
        json = simplejson.loads(r.content)
        self.assertTrue(json['success'])
        self.assertTrue(Comment.objects.filter(name=comment_data['name']).exists())

        # test errors
        comment_data['email'] = 'somebademail'
        r = self.client.post(
            reverse('weblog_comment_add'), comment_data,
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(r.status_code, 200)
        json = simplejson.loads(r.content)
        self.assertFalse(json['success'])
        self.assertTrue(len(json['errors']))
