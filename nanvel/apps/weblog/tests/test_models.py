from django_any import any_model

from django.conf import settings
from django.core import mail
from django.core.urlresolvers import reverse
from django.test import TestCase

from ..models import WeblogItem, Comment, Tag


class WeblogItemModelTestCase(TestCase):

    def setUp(self):
        self.weblog_item = any_model(WeblogItem)

    def test_unicode(self):
        self.assertEqual(str(self.weblog_item), self.weblog_item.title)

    def test_cut_and_part2(self):
        """
        part2 returns post content after POST_CUT_DIVIDER
        """
        # without cut
        without_cut = 'Some content without cut'
        self.weblog_item.content = without_cut
        self.weblog_item.save()
        self.assertEqual(self.weblog_item.cut, without_cut)
        self.assertEqual(self.weblog_item.part2, u'')
        # with cut
        before = 'Text before'
        after = ' text after'
        divider = settings.POST_CUT_DIVIDER
        self.weblog_item.content = u'%s%s%s' % (before, divider, after)
        self.weblog_item.save()
        self.assertEqual(self.weblog_item.cut, before)
        self.assertEqual(self.weblog_item.part2, '%s%s' % (divider, after))

    def test_get_absolute_url(self):
        self.assertEqual(
            self.weblog_item.get_absolute_url(),
            reverse(
                'weblog_post_full',
                kwargs={'slug': self.weblog_item.slug}))

    def test_has_full(self):
        divider = settings.POST_CUT_DIVIDER
        self.weblog_item.content=u'Some content'
        self.weblog_item.save()
        self.assertFalse(self.weblog_item.has_full())
        self.weblog_item.content = (
            u'Some content %s Some content 2' % divider)
        self.weblog_item.save()
        self.assertTrue(self.weblog_item.has_full())

    def test_tags(self):
        wi = WeblogItem(
            content='Some content', title='Some title',
            slug='some_slug', status=WeblogItem.ACTIVE)
        wi.save()
        self.assertFalse(wi.tags.all())
        tag = Tag.objects.create(name='Some tag name')
        wi.tags.add(tag)
        self.assertTrue(wi.tags.all())

    def test_comment_count(self):
        """
        return '' if no moderated comments
        return ' (<comments count>)' othervice
        """
        self.assertEqual(self.weblog_item.comments.count(), 0)
        self.comment = any_model(
            Comment, weblog_item=self.weblog_item,
            status=Comment.UNMODERATED)
        self.assertEqual(self.weblog_item.comments_count, '')
        self.comment.status = Comment.GOOD
        self.comment.save()
        self.assertEqual(self.weblog_item.comments_count, ' (1)')


class CommentModelTestCase(TestCase):

    def setUp(self):
        self.comment = any_model(Comment)

    def test_unicode(self):
        content = 'a' * 250
        self.comment.content = content
        self.comment.save()
        self.assertEqual(str(self.comment), '%s...' % content[:47])
        content = 'abc '
        self.comment.content = content * 10
        self.comment.save()
        self.assertEqual(str(self.comment), '%s...' % (content * 5))

    def test_comment_manager(self):
        self.comment.status = Comment.UNMODERATED
        self.comment.save()
        self.assertFalse(Comment.objects.active().filter(
            pk=self.comment.pk).exists())
        self.comment.status = Comment.GOOD
        self.comment.save()
        self.assertTrue(Comment.objects.active().filter(
            pk=self.comment.pk).exists())

    def test_send_new_comment_mail(self):
        """
        When new comment added,
        email should be sended to MANAGERS
        """
        with self.settings(MANAGERS=(
                ('Yui', 'yui@mail.com'),
                ('Ui', 'ui@mail.com'))):
            mail.outbox = []
            comment = any_model(Comment)
            self.assertEqual(len(mail.outbox), 1)
            self.assertNotEqual(
                mail.outbox[0].subject.find(comment.weblog_item.title), -1)
            self.assertNotEqual(
                mail.outbox[0].body.find(comment.content), -1)
            comment.name = 'new name'
            comment.save()
            # email was not sended
            self.assertEqual(len(mail.outbox), 1)
            


class TagModelTestCase(TestCase):

    def test_unicode(self):
        name = 'Some tag'
        count = Tag.objects.count()
        tag = Tag(name=name, slug='slug')
        tag.save()
        self.assertEqual(Tag.objects.count(), count + 1)
        self.assertEqual(unicode(tag), name)
