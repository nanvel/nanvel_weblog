from django_any import any_model

from django.conf import settings
from django.contrib.flatpages.models import FlatPage
from django.contrib.sites.models import Site
from django.template.base import Template
from django.template.context import Context
from django.test import TestCase

from ..models import Tag, WeblogItem
from ..templatetags.weblog_tags import urlget


class TemplatetagsTestCase(TestCase):

    def test_tags(self):

        def render_tag():
            template = Template('{% load weblog_tags %}'
                                '{% tags %}')
            ctx = Context({})
            return template.render(ctx)

        tag_name = 'Some tag'
        tag_slug = 'some-slug'
        for i in range(2):
            Tag.objects.create(
                name='%s %d' % (tag_name, i),
                slug='%s-%d' % (tag_slug, i))

        result = render_tag()
        for t in Tag.objects.all():
            self.assertNotEqual(result.find(t.name), -1)
            self.assertNotEqual(result.find(t.slug), -1)

    def test_latest_updates(self):

        def render_tag():
            template = Template('{% load weblog_tags %}'
                                '{% latest_updates %}')
            ctx = Context({})
            return template.render(ctx)

        self.assertEqual(WeblogItem.objects.count(), 0)

        for i in range(2):
            any_model(
                WeblogItem, title='post %d' % i,
                status=WeblogItem.ACTIVE)

        result = render_tag()
        for i in WeblogItem.objects.all():
            self.assertNotEqual(result.find(i.get_absolute_url()), -1)
            self.assertNotEqual(result.find(i.title), -1)

    def test_post_tags(self):

        def render_tag(post):
            template = Template('{% load weblog_tags %}'
                                '{% post_tags post %}')
            ctx = Context({'post': post})
            return template.render(ctx)

        tag_name = 'Some tag'
        tag_slug = 'some-slug'
        for i in range(2):
            Tag.objects.create(
                name='%s %d' % (tag_name, i),
                slug='%s-%d' % (tag_slug, i))

        post = any_model(WeblogItem)
        tags = Tag.objects.all()

        result = render_tag(post)
        for t in tags:
            self.assertTrue(result.find(t.name) == -1)
            self.assertTrue(result.find(t.slug) == -1)

        for t in tags:
            post.tags.add(t)

        result = render_tag(post)
        for t in tags:
            self.assertTrue(result.find(t.name) != -1)
            self.assertTrue(result.find(t.slug) != -1)

    def test_projects(self):
        """
        return projects list
        select from flatpages where url starts from settings.PROJECTS_URL
        """

        def render_tag():
            template = Template('{% load weblog_tags %}'
                                '{% projects %}')
            ctx = Context({})
            return template.render(ctx)

        self.assertFalse(FlatPage.objects.count())
        site = Site.objects.get_current()
        fp = FlatPage.objects.create(
            url='/some/url/', title='Some title',
            content='Some content', enable_comments=False,
            registration_required=False)
        fp.sites.add(site)
        self.assertEqual(render_tag(), u'')
        fp = FlatPage.objects.create(
            url='%surl/' % settings.PROJECTS_URL, title='Some title',
            content='Some content', enable_comments=False,
            registration_required=False)
        fp.sites.add(site)
        res = render_tag()
        self.assertNotEqual(res.find(fp.url), -1)
        self.assertNotEqual(res.find(fp.title), -1)


class TemplateFiltersTestCase(TestCase):

    def test_urlget(self):
        var_name = 'q'
        value = 'some query'
        self.assertEqual(urlget(None, var_name), u'')
        self.assertEqual(
            urlget(value, var_name),
            u'&%s=%s' % (var_name, value))
