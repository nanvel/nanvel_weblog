from django.test import TestCase

from ..utils import get_page_number


class WeblogUtilsTestCase(TestCase):

    def test_get_page_number(self):
        test_data_set = [
        {
            'get': {'page': '1'},
            'result': 1
        },
        {
            'get': {'page': 'a'},
            'result': 1
        },
        {
            'get': {},
            'result': 1
        }]

        for data in test_data_set:
            self.assertEqual(
                get_page_number(data['get']), data['result'])
