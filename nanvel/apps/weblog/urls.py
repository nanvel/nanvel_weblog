from django.conf.urls import patterns, url

from .views import LatestPostsFeed


urlpatterns = patterns('nanvel.apps.weblog.views',
    url(r'^feed/$', LatestPostsFeed(), name='weblog_feed'),
    url(r'^comment/add/$', 'comment', name='weblog_comment_add'),
    url(r'^tag/(?P<tag>[-\w]+)/$', 'list', name='weblog_tag'),
    url(r'^(?P<slug>[-\w]+)/$', 'full', name='weblog_post_full'),
    url(r'^(?P<slug>[-\w]+)$', 'full_redirect'),
)
