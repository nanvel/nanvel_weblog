from django.http import Http404, HttpResponse
from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.contrib.syndication.views import Feed
from django.template import RequestContext
from django.core.paginator import Paginator
from django.conf import settings
from django.utils import simplejson

from nanvel.apps.core.templatetags.core_tags import simpledate
from nanvel.apps.core.utils import TodayQuotes

from .models import WeblogItem, Comment, Tag
from .forms import CommentForm, WeblogSearchForm
from .utils import get_page_number


def list(request, tag=None):
    page_n = get_page_number(request.GET)
    items = WeblogItem.objects.active()
    if tag:
        tag = get_object_or_404(Tag, slug=tag)
        items = items.filter(tags=tag)
    q = request.GET.get('q', None)
    if q:
        search_form = WeblogSearchForm(request.GET)
        posts_ids = [post.id for post in search_form.search()]
        print posts_ids
        items = items.filter(id__in=posts_ids)
    paginator = Paginator(items, settings.POSTS_PER_PAGE, orphans=3)
    if page_n > paginator.num_pages:
        raise Http404
    page = paginator.page(page_n)
    phraze = TodayQuotes.get()
    ctx = {
        'page': page, 'tag': tag, 'q': q,
        'phraze': phraze, 'show_ad': False}
    return render_to_response(
        'weblog/list.html', ctx,
        context_instance=RequestContext(request))


def full(request, slug):
    try:
        post = WeblogItem.objects.active().get(slug=slug)
    except WeblogItem.DoesNotExist:
        raise Http404
    if request.is_ajax():
        return HttpResponse(post.part2, mimetype='text/html')
    comment_form = CommentForm(initial={'weblog_item': post.slug})
    comments = Comment.objects.active().filter(weblog_item=post)
    ctx = {
        'post': post, 'comment_form': comment_form,
        'comments': comments, 'show_ad': post.show_ad}
    return render_to_response(
        'weblog/full.html', ctx,
        context_instance=RequestContext(request))


def full_redirect(request, slug):
    return redirect('weblog_post_full', slug)


def comment(request):
    if request.method != 'POST' or not request.is_ajax():
        raise Http404
    form = CommentForm(request.POST)
    if form.is_valid():
        comment = form.save()
        if comment:
            json = simplejson.dumps({'success': True})
            return HttpResponse(json, mimetype='application/json')
    else:
        json = simplejson.dumps({
            'success': False, 'errors': form.errors})
        return HttpResponse(json, mimetype='application/json')


class LatestPostsFeed(Feed):
    title = "Nanvel's blog posts"
    link = "/"
    description = "Latest nanvel's blog posts."

    def items(self):
        return WeblogItem.objects.filter(
            status=WeblogItem.ACTIVE).order_by('-timestamp')[:20]

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return u'<p>{description}</p>Modified: {modified}'.format(
            description=item.description,
            modified=simpledate(item.timestamp))
