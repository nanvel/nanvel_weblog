from haystack import indexes

from django.utils import timezone

from .models import WeblogItem


class WeblogItemIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    id = indexes.IntegerField(model_attr='id')

    def get_model(self):
        return WeblogItem

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.filter(
            timestamp__lte=timezone.now(), status=WeblogItem.ACTIVE)
