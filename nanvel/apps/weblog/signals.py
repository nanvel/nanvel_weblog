from haystack.signals import RealtimeSignalProcessor

from haystack.exceptions import NotHandled

from .models import WeblogItem


class NanvelRealtimeSignalProcessor(RealtimeSignalProcessor):

    def handle_save(self, sender, instance, **kwargs):
        """
        Given an individual model instance, determine which backends the
        update should be sent to & update the object on those backends.
        """
        if sender != WeblogItem:
            return

        if instance.status != WeblogItem.ACTIVE:
            return

        using_backends = self.connection_router.for_write(instance=instance)

        for using in using_backends:
            try:
                index = self.connections[using].get_unified_index().get_index(sender)
                index.update_object(instance, using=using)
            except NotHandled:
                # TODO: Maybe log it or let the exception bubble?
                pass