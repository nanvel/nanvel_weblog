from django.contrib import admin

from .models import UploaderImage, UploaderFile


def preview(obj):
    if not obj.image:
        return u'none'
    return u'<img src="%s" style="max-width: 300px;">' % obj.image.url 
preview.short_description = 'Preview'
preview.allow_tags = True


class UploaderImageAdmin(admin.ModelAdmin):
    list_display = ( 'image', preview, 'alt', 'timestamp')
    fields = ('image', 'alt', 'timestamp')
    readonly_fields = ('timestamp',)
    list_per_page = 20

class UploaderFileAdmin(admin.ModelAdmin):
    list_display = ('file', 'timestamp')
    fields = ('file', 'timestamp')
    readonly_fields = ('timestamp',)
    list_per_page = 50


admin.site.register(UploaderImage, UploaderImageAdmin)
admin.site.register(UploaderFile, UploaderFileAdmin)
