from django.contrib.auth.decorators import login_required
from django.utils import simplejson
from django.http import HttpResponse, Http404

from .forms import UploaderImageForm


@login_required
def image(request):
    form = UploaderImageForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        uploader_image = form.save()
        json = simplejson.dumps({
                    'success': True,
                    'upload': {
                        'links': {
                                'original': uploader_image.image.url},
                        'image': {
                                'width': uploader_image.image.width,
                                'height': uploader_image.image.height}
                    }
                })
        return HttpResponse(json, mimetype='application/json')
    json = simplejson.dumps({'success': False, 'errors': form.errors})
    return HttpResponse(json, mimetype='application/json')
