from django import forms

from .models import UploaderImage


class UploaderImageForm(forms.ModelForm):

    alt = forms.CharField(max_length=50, required=False)

    class Meta:
        model = UploaderImage
