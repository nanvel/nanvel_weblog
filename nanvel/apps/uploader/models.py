import os

from django.db import models


class UploaderImage(models.Model):
    image = models.ImageField(upload_to='images')
    alt = models.CharField(max_length=50, default=u'')
    timestamp = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return u'Image %s' % self.alt

    def delete(self, delete_image=False):
        if delete_image and os.path.exists(self.image.path):
            os.remove(self.image.path)
        super(UploaderImage, self).delete()

    def get_absolute_url(self):
        return self.image.url


class UploaderFile(models.Model):
    file = models.FileField(upload_to='files')
    timestamp = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return u'File %s' % self.file.url

    def delete(self, delete_file=False):
        if delete_file and os.path.exists(self.file.path):
            os.remove(self.file.path)
        super(UploaderFile, self).delete()

    def get_absolute_url(self):
        return self.file.url
