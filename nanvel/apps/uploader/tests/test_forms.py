from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase

from ..models import UploaderImage
from ..forms import UploaderImageForm

from .factories import ImageFactory


class UploaderImageFormTestCase(TestCase):

    def setUp(self):
        self.upload_file = ImageFactory.create()

    def test_uploader_image_form(self):

        alt_text = 'Some alt text'

        data_set = [
            {
                'post_dict': {
                    'alt': alt_text,
                },
                'files_dict': {
                    'image': self.upload_file,
                },
                'success': True,
            },
            {
                'post_dict': {
                },
                'files_dict': {
                    'image': self.upload_file,
                },
                'success': True,
            },
            {
                'post_dict': {
                    'alt': alt_text,
                    'image': 'bad_image_data',
                },
                'files_dict': {
                },
                'success': False,
            },
        ]

        for data in data_set:
            form = UploaderImageForm(data['post_dict'], data['files_dict'])
            self.assertEqual(form.is_valid(), data['success'])

        # test data saved
        data = data_set[0]
        form = UploaderImageForm(data['post_dict'], data['files_dict'])
        uploader_image = form.save()
        self.assertTrue(uploader_image.image)
        self.assertEqual(uploader_image.alt, alt_text)

    def tearDown(self):
        # remove saved images from disk
        for uploader_image in UploaderImage.objects.all():
            uploader_image.delete(delete_image=True)
