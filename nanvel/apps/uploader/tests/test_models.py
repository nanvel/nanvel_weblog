import os

from django_any import any_model

from django.conf import settings
from django.core.files.images import File, ImageFile
from django.test import TestCase

from ..models import UploaderImage, UploaderFile

from .factories import ImageFactory


class UploaderImageModelTestCase(TestCase):

    def setUp(self):
        image_file = ImageFile(ImageFactory.create())
        self.alt = 'some alt'
        self.image = any_model(
            UploaderImage, image=image_file,
            alt=self.alt)

    def test_unicode(self):
        self.assertEqual(str(self.image), u'Image %s' % self.alt)

    def test_delete(self):
        count = UploaderImage.objects.count()
        path = self.image.image.path
        self.assertTrue(os.path.exists(path))
        # remove image from disk
        self.image.delete(delete_image=True)
        self.assertEqual(UploaderImage.objects.count(), count - 1)
        self.assertFalse(os.path.exists(path))

    def tearDown(self):
        # remove saved images from disk
        for uploader_image in UploaderImage.objects.all():
            uploader_image.delete(delete_image=True)


class UploaderFileModelTestCase(TestCase):

    def setUp(self):
        image_file = ImageFile(ImageFactory.create())
        self.file = any_model(UploaderFile, file=image_file)

    def test_unicode(self):
        self.assertEqual(str(self.file), u'File %s' % self.file.file.url)

    def test_delete(self):
        count = UploaderFile.objects.count()
        path = self.file.file.path
        self.assertTrue(os.path.exists(path))
        # remove file from disk
        self.file.delete(delete_file=True)
        self.assertEqual(UploaderFile.objects.count(), count - 1)
        self.assertFalse(os.path.exists(path))

    def tearDown(self):
        # remove saved images from disk
        for uploader_file in UploaderFile.objects.all():
            uploader_file.delete(delete_file=True)
