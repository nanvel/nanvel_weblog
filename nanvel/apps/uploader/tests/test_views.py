from django.conf import settings
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.utils import simplejson

from ..models import UploaderImage

from .factories import ImageFactory


class ImageUploadTestCase(TestCase):

    user_credentials = {
        'username': 'uasmi',
        'password': 'uasmi',
        'email': 'usami@mail.com'
    }

    def test_image(self):
        image = ImageFactory.create()

        post_dict = {
            'image': image,
        }

        # anonimous user
        r = self.client.post(reverse('uploader_image'), post_dict)
        self.assertEqual(r.status_code, 302)

        u = User.objects.create_user(**self.user_credentials)
        r = self.client.login(
            username=self.user_credentials['username'],
            password=self.user_credentials['password'])
        self.assertTrue(r)

        # update post data
        image.close()
        image = ImageFactory.create()
        post_dict = {
            'image': image,
        }
        r = self.client.post(reverse('uploader_image'), post_dict)
        self.assertEqual(r.status_code, 200)
        json = simplejson.loads(r.content)
        self.assertTrue(json['success'])
        uploader_image = UploaderImage.objects.order_by('-id')[0]
        self.assertEqual(
            json['upload']['links']['original'],
            uploader_image.image.url)
        self.assertEqual(
            json['upload']['image']['width'],
            uploader_image.image.width)
        self.assertEqual(
            json['upload']['image']['height'],
            uploader_image.image.height)

    def tearDown(self):
        # remove saved images from disk
        for uploader_image in UploaderImage.objects.all():
            uploader_image.delete(delete_image=True)
