import Image

from StringIO import StringIO


class ImageFactory(object):

    counter = 0

    @classmethod
    def create(self, width=200, height=200):
        """
        Returns image file with specified size.
        """
        self.counter += 1
        image = Image.new(
            'RGBA', size=(width, height), color=(256, 0, 0))
        f = StringIO()
        image.save(f, 'png')
        f.name = 'testimage%d.png' % self.counter
        f.seek(0)
        return f