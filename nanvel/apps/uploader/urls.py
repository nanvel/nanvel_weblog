from django.conf.urls import patterns, url


urlpatterns = patterns('nanvel.apps.uploader.views',
    url(r'^image/$', 'image', name='uploader_image'),
)
