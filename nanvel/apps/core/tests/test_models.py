import datetime
import os

from django.conf import settings
from django.core.files.images import ImageFile
from django.test import TestCase

from ..models import Link


class LinkModelTestCase(TestCase):

    def setUp(self):
        self.link = Link(
            url='http://someurl.com', title='Some url')

    def test_unicode(self):
        self.assertEqual(unicode(self.link), self.link.title)

    def test_html(self):
        self.assertEqual(
            self.link.html(),
            u'<a href="%s" target="_blank">%s</a>' % (self.link.url, self.link.title))
