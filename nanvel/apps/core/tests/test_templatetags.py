import datetime

from django.test import TestCase
from django.template.base import Template
from django.template.context import Context

from ..models import Link

from ..templatetags.core_tags import simpledate


class TemplatetagsTestCase(TestCase):

    def test_gravatar_url(self):

        def render_tag(email):
            template = Template('{% load gravatar %}'
                                '{% gravatar email %}')
            ctx = Context({'email': email})
            return template.render(ctx)

        # if email was not submitted, don't link to gravatar profile
        self.assertTrue(
                u'<a href="http://www.gravatar.com/"' in render_tag(u''))
        self.assertTrue(
                u'<a href="http://www.gravatar.com/"' in render_tag(None))
        res = render_tag(u'somemail@mail.com')
        self.assertFalse(u'<a href="http://www.gravatar.com/"' in res)
        self.assertTrue(u'<img src="http://www.gravatar.com' in res)

    def test_links(self):

        def render_tag():
            template = Template('{% load core_tags %}'
                                '{% links %}')
            ctx = Context({})
            return template.render(ctx)

        self.assertEqual(Link.objects.count(), 0)
        self.assertEqual(render_tag(), u'')
        link = Link.objects.create(
                url='http://someurl.com',
                title='Some url')
        res = render_tag()
        self.assertNotEqual(res.find(link.url), -1)
        self.assertNotEqual(res.find(link.title), -1)


class FiltersTestCase(TestCase):

    def test_simpledate(self):
        today = datetime.date.today()
        self.assertEqual(simpledate(today), 'Today')
        self.assertEqual(
                    simpledate(today - datetime.timedelta(days=1)),
                    'Yesterday')
        date = today - datetime.timedelta(days=10)
        self.assertEqual(
                    simpledate(date),
                    datetime.datetime.strftime(date, '%b %d, %Y'))
        # from future
        date = today + datetime.timedelta(days=10)
        self.assertEqual(
                    simpledate(date),
                    datetime.datetime.strftime(date, '%b %d, %Y'))
        # test datetime
        self.assertEqual(simpledate(datetime.datetime.now()), 'Today')
