import datetime

from mock import patch

from django.conf import settings
from django.core import mail
from django.test import TestCase
from django.utils import timezone

from ..models import Quote
from ..utils import send_mail, TodayQuotes


class CoreUtilsTestCase(TestCase):

    def test_send_mail(self):
        mail.outbox = []
        text = u'Some text'
        emails = ('yui@mail.com',)
        send_mail('test/test', {'var': text}, emails)
        self.assertEqual(len(mail.outbox), 1)
        self.assertNotEqual(mail.outbox[0].subject.find(text), -1)
        self.assertNotEqual(mail.outbox[0].body.find(text), -1)

    def test_today_quotes(self):
        self.assertEqual(TodayQuotes.get(), '')
        q1 = Quote.objects.create(text='Some quote 1')
        q2 = Quote.objects.create(text='Some quote 2')
        self.assertEqual(TodayQuotes.get(), q2.text)
        self.assertEqual(TodayQuotes.get(), q2.text)
        # after some time
        with patch.object(timezone, 'now', 
                return_value=timezone.now() + datetime.timedelta(
                    hours=settings.CHANGE_QUOTE_PERIOD + 1)) as mock_now:
            self.assertEqual(TodayQuotes.get(), q1.text)
            self.assertEqual(TodayQuotes.get(), q1.text)
