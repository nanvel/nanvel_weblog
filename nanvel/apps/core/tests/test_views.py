from django.conf import settings
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.test.client import RequestFactory

from nanvel import urls

from ..views import handler404, handler500


class ErrorHandlersTestCase(TestCase):

    def test_http_404(self):
        # test handler connected
        self.assertTrue(urls.handler404)
        # test handlers works
        factory = RequestFactory()
        request = factory.get('/')
        r = handler404(request)
        self.assertEqual(r.status_code, 404)
        self.assertNotEqual(unicode(r).find('Page not found'), -1)
        # and one more test, just for fun
        with self.settings(DEBUG=False):
            r = self.client.get('/not/exist/page/')
            self.assertEqual(r.status_code, 404)
            self.assertNotEqual(unicode(r).find('Page not found'), -1)

    def test_http_500(self):
        # test handler connected
        self.assertTrue(urls.handler500)
        # test handlers works
        factory = RequestFactory()
        request = factory.get('/')
        r = handler500(request)
        self.assertEqual(r.status_code, 500)
        self.assertNotEqual(unicode(r).find('Server internal error'), -1)


class CoreViewsTestCase(TestCase):

    def test_robots(self):
        with self.settings(ADMIN_URL='neadminurl/'):
            r = self.client.get(reverse('robots'))
            self.assertEqual(r.status_code, 200)
            self.assertContains(r, 'admin', 3)
            self.assertContains(r, 'Goolebot')
            self.assertContains(r, settings.ADMIN_URL.split('/')[0])
