from django.test import TestCase
from django.template import RequestContext
from django.test.client import RequestFactory
from django.conf import settings


class ContextProcessorsTestCase(TestCase):

    def test_ga_id(self):
        # test variable exists
        settings.GA_ID
        # test context_processor
        ga_id = 'GA123456'
        with self.settings(GA_ID = ga_id):
            factory = RequestFactory()
            request = factory.get('/')
            context = RequestContext(request)
            self.assertEqual(context['GA_ID'], ga_id)
