import os, datetime

from django.db import models


class Link(models.Model):
    """
    'May be interesting for You' section
    """
    url = models.URLField()
    title = models.CharField(max_length=50)
    order = models.PositiveIntegerField(default=0)

    def __unicode__(self):
        return self.title

    def html(self):
        return u'<a href="%s" target="_blank">%s</a>' % (
            self.url, self.title)


class Quote(models.Model):
    text = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True, editable=False)

    class Meta:
        ordering = ('-timestamp',)

    def __unicode__(self):
        return self.text[:100]
