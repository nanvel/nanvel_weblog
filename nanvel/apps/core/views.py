from django.template import loader, Context
from django.conf import settings
from django.http import (
    HttpResponseServerError, HttpResponseNotFound,
    HttpResponse)


def handler500(request, template_name='500.html'):
    t = loader.get_template(template_name)
    return HttpResponseServerError(t.render(Context({})))


def handler404(request, template_name='404.html'):
    t = loader.get_template(template_name)
    return HttpResponseNotFound(t.render(Context({})))


def robots(request):
    t = loader.get_template('robots.txt')
    admin_url = '/'.join([i for i in settings.ADMIN_URL.split('/') if i])
    return HttpResponse(
        t.render(Context({'admin_url': admin_url})),
        'text/plain')
