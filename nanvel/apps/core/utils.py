import datetime

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils import timezone

from .models import Quote


def send_mail(template, context, emails, from_email=None):
    if not from_email:
        from_email = settings.DEFAULT_FROM_EMAIL
    text_content = render_to_string('%s.txt' % template, context)
    html_content = render_to_string('%s.html' % template, context)
    subject = render_to_string('%s_subject.txt' % template , context)
    subject = ' '.join(subject.split('\n'))
    msg = EmailMultiAlternatives(subject, text_content, from_email, emails)
    msg.attach_alternative(html_content, "text/html")
    msg.send()


class TodayQuotes:

    current = 0
    changed = None
    quote = ''

    @classmethod
    def get(self):
        if (not self.changed or
            self.changed < timezone.now() - datetime.timedelta(
                hours=settings.CHANGE_QUOTE_PERIOD)):
            # get next quote
            count = Quote.objects.count()
            if count > 0:
                if self.current >= count:
                    self.current = 0
                self.quote = Quote.objects.all()[self.current].text
                self.current += 1
                self.changed = timezone.now()
        return self.quote
