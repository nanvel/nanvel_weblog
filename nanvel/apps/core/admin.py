import datetime

from django.contrib import admin

from .models import Link, Quote


class LinkAdmin(admin.ModelAdmin):
    list_display = ('title', 'order',)


def quote_text(obj):
    return obj.text
quote_text.allow_tags = True
quote_text.short_description = 'Quote'


class QuoteAdmin(admin.ModelAdmin):
    list_display = ('id', quote_text, 'timestamp',)
    search_fields = ('text',)
    list_per_page = 20


admin.site.register(Link, LinkAdmin)
admin.site.register(Quote, QuoteAdmin)
