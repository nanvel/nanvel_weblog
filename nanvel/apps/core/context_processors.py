from django.conf import settings


def common(request):
	return {
        'GA_ID': settings.GA_ID,
        'SHARE_THIS_ID': settings.SHARE_THIS_ID,
        'USE_JS': getattr(settings, 'USE_JS', True), # to disable js while development
	}
