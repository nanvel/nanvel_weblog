import datetime

from django import template

from nanvel.apps.core.models import Link


register = template.Library()


@register.filter
def simpledate(value):
    """
    return today or yesturday or date
    """
    if isinstance(value, datetime.datetime):
        date = value.date()
    else:
        date = value
    today = datetime.date.today()
    if date < today - datetime.timedelta(days=1) or date > today:
        return datetime.datetime.strftime(date, '%b %d, %Y')
    if date == today:
        return 'Today'
    return 'Yesterday'


@register.simple_tag
def links():
    links = Link.objects.all()
    if not links.exists():
        return u''
    html = u'<ul>'
    for l in links:
        html = u'%s<li>%s' % (html, l.html())
    return u'%s</ul>' % html
