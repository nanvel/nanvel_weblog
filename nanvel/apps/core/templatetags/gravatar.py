import urllib, hashlib

from django import template
from django.conf import settings


register = template.Library()


class GravatarNode(template.Node):

    def __init__(self, email):
        self.email = template.Variable(email)

    def render(self, context):
        email = u''
        try:
            email = self.email.resolve(context) or u''
        except template.VariableDoesNotExist:
            pass
        size = 60
        gravatar_hash = hashlib.md5(email.lower()).hexdigest()
        gravatar_avatar = 'http://www.gravatar.com/avatar/' + gravatar_hash + '?'
        gravatar_avatar += urllib.urlencode({'s':str(size)})
        if email:
            return '<a href="%s%s" target="_blank"><img src="%s"></a>' % (
                                'http://www.gravatar.com/',
                                gravatar_hash,
                                gravatar_avatar) 
        return '<a href="%s" target="_blank"><img src="%s"></a>' % (
                                'http://www.gravatar.com/',
                                gravatar_avatar)


@register.tag
def gravatar(parser, token):
    try:
        tag_name, email = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError, "%r tag requires a single argument" % token.contents.split()[0]
    return GravatarNode(email)
