(function($, undefined){
    $(function(){
        var $link = $('<div/>').addClass('link-move-top').appendTo('body');
        $(window).scroll(function(data){
            show_move_top_link(this, $link);
        });
        $(window).resize(function(){
            show_move_top_link(this, $link);
        });
        $link.click(function(){
            $('html, body').animate({
                scrollTop: 0,
            }, 500);
        });
        setTimeout(generate_background, 1000);
    });
    function show_move_top_link(obj, link){
        if($(obj).width() > 1000){
            if(obj.pageYOffset > 1000){
                link.fadeIn(500);
            } else {
                link.fadeOut(500);
            }
        } else {
            link.fadeOut(500);
        }
    };
    /* generate background */
    function insert_circle(radius, top, left) {
        $body = $('body');
        var $circle = $('<div/>').css({
            width: radius + 'px', height: radius + 'px',
            top: top + 'px', left: left + 'px',
            'background-color': 'rgba(' + 
                Math.round(Math.random() * 255) + ',' +
                Math.round(Math.random() * 255) + ',' +
                Math.round(Math.random() * 255) + ',' +
                Math.random() * 0.2 +')'}).addClass('bgcircle');
        $circle.appendTo($body);
    }
    function generate_background() {
        var radius, top, left;
        var max_radius = Math.min($(window).width(), $(document).height());
        for(var i=0; i<20; i++) {
            radius = Math.random() * 200 + 50;
            top = ($(document).height() - radius) / 20 * (Math.random() + i);
            left = Math.random() * 150;
            insert_circle(radius, top, left);
        }
    }
})(jQuery);
