(function($, undefined) {
    $(function() {
        // forms validation
        $('.js-comment-form').validate({
            rules: {
                name: 'required',
                content: 'required',
                email: {
                    email: true
                },
                site: {
                    url: true
                }
            },
            //messages: {
            //    site: {
            //        required: "This field is required",
            //        email: "Please enter a valid email address"
            //    },
            //},
            errorClass: "error",
            errorElement: "div",
            errorPlacement: function(error, element) {
                $(error).addClass('alert alert-danger').insertBefore(element);
            },
            submitHandler: function(form) {
                var $form = $(form);
                var $button = $form.find('button');
                if($button.hasClass('disabled')) return false;
                $.ajax({
                    url: $form.attr('action'),
                    type: 'post',
                    dataType: "json",
                    data: $form.serialize(),
                    success: function (data) {
                        if(data['success']) {
                            success_alert($form, true);
                        } else {
                            success_alert($form, false, data['errors']);
                        }
                    },
                    error: function() {
                        success_alert($form, false);
                    },
                    beforeSend: function() {
                        $button.addClass('disbled');
                    },
                    complete: function() {
                        $button.removeClass('disbled');
                    },
                });
                return false;
            }
        });
        $('pre > code').addClass('prettyprint');
        prettyPrint();
    });

    function success_alert(form, success, errors) {
        var errors_string = '';
        if(errors) {
            for(var error in errors) {
                errors_string += '<br>' + error + ': ' + errors[error];
            }
        }
        $('.alert').remove();
        if (success) {
            $('<div/>').addClass('alert alert-success result')
            .text('Comment will be published after moderating. Thanks for your interest!')
            .insertBefore($('#id_name'));
            form.find('input[type=text], textarea').val('');
        } else {
            $('.alert').remove();
            $('<div/>').addClass('alert alert-danger result')
            .html('Something going wrong (.' + errors_string)
            .insertBefore($('#id_name'));
        }
    }
})(jQuery);
