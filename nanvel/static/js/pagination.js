(function($, undefined){

    $(function(){
        var path = '';
        var current = 1;
        var MIN_SIDE = 3;
        var MIN_CENTER = 3;

        page_item = function(n, title) {
            var html = '<li>';
            if(n==current){
                html = '<li class="active">'
            }
            html += '<a href="' + path + 'page=' + n + '">' + title + '</a></li>';
            return $(html)
        }

        var $paginator = $('.pagination');
        if(!$paginator.length) {
            return
        };
        path = window.location.pathname;
        var attrs = window.location.search;
        if(attrs.length){
            attrs = attrs.slice(1).split('&');
            for(var attr='', i=0; i<attrs.length; i++) {
                attr = attrs[i].split('=');
                if(attr[0]=='page') {
                    current = parseInt(attr[1]);
                    attrs.splice(i, 1);
                    break;
                }
            }
        }
        path += '?';
        if(attrs.length) {
            path += attrs.join('&') + '&';
        }
        var pages = parseInt($paginator.attr('data-pages'));
        var $ul = $paginator;
        if(current>1) {
            $ul.append(page_item(current - 1, '&larr;'));
        }
        if(pages<MIN_SIDE * 2 + MIN_CENTER + 2) {
            for(var i=1, html=''; i<=pages; i++) {
                $ul.append(page_item(i, i));
            }
        } else if(current<MIN_SIDE + 1) {
            for(var i=1, html=''; i<=MIN_SIDE + 1; i++) {
                $ul.append(page_item(i, i));
            }
            $ul.append($('<li><span>...</span></li>'));
            for(var i=pages - MIN_SIDE - MIN_CENTER, html=''; i<=pages; i++) {
                $ul.append(page_item(i, i));
            }
        } else if(current>pages - MIN_SIDE) {
            for(var i=1, html=''; i<=MIN_SIDE + MIN_CENTER; i++) {
                $ul.append(page_item(i, i));
            }
            $ul.append($('<li><span>...</span></li>'));
            for(var i=pages - MIN_CENTER, html=''; i<=pages; i++) {
                $ul.append(page_item(i, i));
            }
        } else {
            for(var i=1, html=''; i<=MIN_SIDE; i++) {
                $ul.append(page_item(i, i));
            }
            $ul.append($('<li><span>...</span></li>'));
            for(var i=current - Math.round(MIN_CENTER / 2), html=''; i<=current + Math.round(MIN_CENTER / 2); i++) {
                $ul.append(page_item(i, i));
            }
            $ul.append($('<li><span>...</span></li>'));
            for(var i=pages - MIN_SIDE + 1, html=''; i<=pages; i++) {
                $ul.append(page_item(i, i));
            }
        }
        if(current<pages) {
            $ul.append(page_item(current + 1, '&rarr;'));
        }
    });

})(jQuery);
