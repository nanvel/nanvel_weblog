from django.conf import global_settings as DEFAULT_SETTINGS


MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.contrib.redirects.middleware.RedirectFallbackMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = \
                        DEFAULT_SETTINGS.TEMPLATE_CONTEXT_PROCESSORS + (
    'nanvel.apps.core.context_processors.common',
)

ROOT_URLCONF = 'nanvel.urls'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.flatpages',
    'django.contrib.sitemaps',
    'django.contrib.redirects',
    'south',
    'pipeline',
    'haystack',
    'nanvel.apps.core',
    'nanvel.apps.weblog',
    'nanvel.apps.uploader',
)
