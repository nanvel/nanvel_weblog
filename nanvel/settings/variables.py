# security through obscurity, default: admin/
ADMIN_URL = 'admin/'

# posts per page
POSTS_PER_PAGE = 5

# post cut divider
POST_CUT_DIVIDER = u'<div id="cut"></div>'

# used in projects templatetag
# all projects flatpages urls have to start from it
PROJECTS_URL = '/projects/'

# specify it in settings_local.py to use Google Analytics
GA_ID = ''
# GA credentials (used in analytics app)
GA_PROFILE_ID = 0
GA_SOURCE_APP_NAME = ''
GA_USERNAME = ''
GA_PASSWORD = ''

SHARE_THIS_ID = ''

DEFAULT_FROM_EMAIL = 'nanvel@nanvel.name'

# change quote after hours
CHANGE_QUOTE_PERIOD = 1
