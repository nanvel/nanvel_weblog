
PIPELINE_CSS = {
    'common': {
        'source_filenames': (
            'css/bootstrap.less',
            'css/common.less',
        ),
        'output_filename': 'css/common.min.css',
    },
    'weblog': {
        'source_filenames': (
            'libs/prettify/prettify.css',
            'css/weblog.less',
        ),
        'output_filename': 'css/weblog.min.css',
    },
    'flatpage': {
        'source_filenames': (
            'css/flatpage.less',
        ),
        'output_filename': 'css/flatpage.min.css',
    },
}

PIPELINE_JS = {
    'common': {
        'source_filenames': (
            'js/jquery.js',
            'js/common.js',
        ),
        'output_filename': 'js/common.min.js',
    },
    'nicedit': {
        'source_filenames': (
            'js/nicedit.js',
        ),
        'output_filename': 'js/nicedit.min.js',
    },
    'weblog': {
        'source_filenames': (
            'js/jquery.validate.js',
            'libs/prettify/prettify.js',
            'js/pagination.js',
            'js/weblog.js',
        ),
        'output_filename': 'js/weblog.min.js',
    },
}

# When PIPELINE is True, CSS and JavaScripts will be concatenated and filtered.
# When False, the source-files will be used instead.
# Default: PIPELINE = not DEBUG

PIPELINE_CSS_COMPRESSOR = 'pipeline.compressors.yui.YUICompressor'
PIPELINE_JS_COMPRESSOR = 'pipeline.compressors.yui.YUICompressor'

PIPELINE_YUI_BINARY = '/usr/bin/yui-compressor'

PIPELINE_COMPILERS = (
  'nanvel.apps.core.compilers.RubyLesscCompiler',
)

PIPELINE_LESS_BINARY = '/usr/local/bin/lessc'

PIPELINE_DISABLE_WRAPPER = True
