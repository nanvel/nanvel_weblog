import os.path


def absolute_path(name):
    return os.path.join(os.path.dirname(__file__), '../%s' % name)
