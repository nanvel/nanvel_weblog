import datetime

from os import environ, path

from django.core.exceptions import ImproperlyConfigured

from fabric.api import run, sudo, cd, env, get


DUMPS_DIR = '/Users/nanvel/myprojects/dumps/nanvel.name/'

PROJECT_ROOT = '/home/nanvel/deploy/nanvel_weblog'
MEDIA_ROOT = '/home/nanvel/nanvel.name/media'
SERVER = 'nanvel.name'
USER = 'nanvel'
PASSWORD_ENV_VAR = 'NANVEL_NAME_PASS'

env.user = USER
env.hosts = [SERVER]

try:
    env.password = environ[PASSWORD_ENV_VAR]
except KeyError:
    raise ImproperlyConfigured('%s is not specified' % PASSWORD_ENV_VAR)


def deploy():
    with cd(PROJECT_ROOT):
        run('git pull origin master')
        run('.env/bin/python manage.py migrate')
        run('.env/bin/python manage.py collectstatic --noinput')
        sudo('/etc/init.d/fastcgi restart')

def get_db_dump():
    with cd(PROJECT_ROOT):
        run('bash dump.sh')
        get('dump.gz', path.join(DUMPS_DIR,
                datetime.datetime.strftime(
                        datetime.datetime.now(), 'db_%Y%m%d_%H%M%S.dump.gz')))

def get_media_dump():
    zip_file = '%s.zip' % MEDIA_ROOT
    run('zip -r %s %s' % (zip_file, MEDIA_ROOT))
    get(zip_file, path.join(DUMPS_DIR,
            datetime.datetime.strftime(
                    datetime.datetime.now(), 'media_%Y%m%d_%H%M%S.zip')))
    run('rm %s' % zip_file)
